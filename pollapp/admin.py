from django.contrib import admin
from .models import CustomUser, OTP, Candidates, VotingTime


admin.site.register(CustomUser)
admin.site.register(OTP)
admin.site.register(Candidates)
admin.site.register(VotingTime)
