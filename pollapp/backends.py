from django.contrib.auth.backends import BaseBackend
from django.core.exceptions import ValidationError
from .models import OTP, CustomUser


class CustomBackend(BaseBackend):
    def authenticate(self, request, email=None, otp=None, **kwargs):
        if not CustomUser.objects.filter(email=email):
            raise ValidationError("user not found")
        user = CustomUser.objects.get(email=email)
        user_otp = OTP.objects.get(user=user)

        if user_otp:
            if user_otp.otp_code == int(otp):
                user_otp.used = True
                user_otp.save()
                return user
            else:
                return False

    def get_user(self, user_id):
        try:
            return CustomUser.objects.get(id=user_id)

        except CustomUser.DoesNotExist:
            return None
