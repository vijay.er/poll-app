from django import forms
from multiselectfield import MultiSelectFormField
from pollapp.models import Candidates, name_list, VotingTime


class NameList(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(NameList, self).__init__(*args, **kwargs)
        self.fields['candidates_name'] = forms.MultipleChoiceField(choices=name_list, widget=forms.CheckboxSelectMultiple())

    def clean_candidates_name(self):
        if not len(self.cleaned_data['candidates_name']) == 3:
            raise forms.ValidationError('Select 3 option.')
        return self.cleaned_data['candidates_name']

    class Meta:
        model = Candidates
        fields = ['candidates_name']


class DateTimeInput(forms.DateTimeField):
    input_type = 'datetime-local'


class VoteTimingForm(forms.ModelForm):

    class Meta:
        model = VotingTime
        fields = ['start_time', 'end_time']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["start_time"].widget = DateTimeInput()
        self.fields["start_time"].input_formats = ["%Y-%m-%dT%H:%M", "%Y-%m-%d %H:%M"]
        self.fields["end_time"].widget = DateTimeInput()
        self.fields["end_time"].input_formats = ["%Y-%m-%dT%H:%M", "%Y-%m-%d %H:%M"]