from django.contrib.auth.models import AbstractUser
from django.db import models
from multiselectfield import MultiSelectField


class CustomUser(AbstractUser):
    email = models.EmailField(unique=True)
    username = models.CharField(blank=True, null=True, max_length=100)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.email


class OTP(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    email = models.CharField(max_length=200, blank=True, null=True)
    otp_code = models.IntegerField(blank=True, null=True)
    used = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.user)


name_list = (
    ('Shreya', 'Shreya'),
    ('Ravi', 'Ravi'),
    ('Divya Bharathi', 'Divya Bharathi'),
    ('Vishnu', 'Vishnu'),
    ('Mohanlal', 'Mohanlal'),
    ('Megha', 'Megha'),

)


class Candidates(models.Model):
    candidates_name = MultiSelectField(choices=name_list, max_length=100)
    voted_by = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.candidates_name)


class VotingTime(models.Model):
    start_time = models.DateTimeField(max_length=100)
    end_time = models.DateTimeField(max_length=100)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return str(self.is_active)