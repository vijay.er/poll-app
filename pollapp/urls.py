from django.urls import path
from .views import create_user, index, Otp_request, verify_otp, submitvote, count, userlogout

urlpatterns = [
    path('', index, name='home'),
    path('create/', create_user),
    path('login/', Otp_request, name='login'),
    path('otp/', verify_otp, name='otpverify'),
    path('voting/', submitvote, name='submit'),
    path('count/', count, name='count'),
    path('logout/', userlogout, name='logout'),

]
