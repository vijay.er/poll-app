from django.contrib.auth import get_user_model
from django.core.mail import send_mail
import math
import random
from deepsense import settings
from pollapp.models import OTP

User = get_user_model()


def generate_otp():
    """
    Generates a random x digit number from 0-9
    """
    otp_code_length = 6
    digits = "123456789"
    otp = ""

    for i in range(otp_code_length):
        otp += digits[math.floor(random.random() * 9)]

    return otp


def otp_creation(user_data, email):
    """
    Generating an OTP code from other function
    updating in the user otp if user exists, unless creating a new otp for user

    :param user_data: user data for creating otp
    :param email: Users Email Id
    :return: generated OTP code
    """
    code = generate_otp()
    if user_data.otp_set.values('otp_code'):
        otp_obj = OTP.objects.filter(user__email=email).first()
        otp_obj.otp_code = code
        otp_obj.used = False
        otp_obj.save(force_update=True)
        send_otp(otp_code=code, email=email)
        return otp_obj.otp_code
    else:
        otp_code = OTP.objects.create(
            user=user_data,
            otp_code=code
        )
        send_otp(otp_code=code, email=email)
        return otp_code


def send_otp(otp_code, email):
    """
    Sending OTP to the user

    :param email: To which email OTP has to send
    :param otp_code: OTP code for a user
    :return:
    """
    username = User.objects.get(email=email)
    subject = "OTP for your login"

    message = str(
        "Your verification OTP is " + otp_code)
    send_mail(subject, message,
              settings.FROM_EMAIL,
              [email],
              fail_silently=False, )
    return username
