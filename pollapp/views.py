import datetime
from collections import Counter
from itertools import chain
import pytz
from django.contrib.auth import get_user_model, authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from deepsense import settings
import uuid
from pollapp.forms import NameList, VoteTimingForm
from pollapp.models import OTP, Candidates, VotingTime
from pollapp.utils import otp_creation
from datetime import datetime, timedelta

User = get_user_model()


def index(request):
    now = datetime.now()

    voting_time = VotingTime.objects.get(is_active=True)
    start_timing = voting_time.start_time + timedelta(hours=5, minutes=30)
    start_time_format = start_timing.strftime('%m/%d/%Y %H:%M:%S')
    end_timing = voting_time.end_time + timedelta(hours=5, minutes=30)
    end_time_format = end_timing.strftime('%m/%d/%Y %H:%M:%S')

    current_time = now.strftime('%m/%d/%Y %H:%M:%S')
    # if start_time_format < current_time < end_time_format:
    #     vote = "start"
    #     return render(request, 'basic_auth/login.html', {'data': vote})
    print(request.user)
    if start_time_format > current_time:
        vote = "Voting start at " + start_timing.strftime('%d') + \
               'th ' + start_timing.strftime('%B') + ' at ' + start_timing.strftime("%I%p")
        return render(request, 'basic_auth/home.html', {'voting_time': vote})
    if start_time_format < current_time:

        if current_time > end_time_format:
            vote = "Voting finished"
            return render(request, 'basic_auth/home.html', {'voting_finished': vote})

        vote = "Voting already started"
        return render(request, 'basic_auth/home.html', {'voting_time': vote})

    return render(request, 'basic_auth/home.html')


def create_user(request):
    userlist = settings.USERLIST

    for i in userlist:
        old_user = User.objects.filter(email=i)
        if not old_user:
            user = User()
            user.is_active = True
            pwd = str(uuid.uuid4())
            user.email = i
            user.set_password(pwd)
            user.save()
        else:
            pass
    return HttpResponse("done")


@csrf_exempt
def Otp_request(request):
    if request.method == 'POST':
        email = request.POST['email']
        verify_user = User.objects.filter(email=email)
        if verify_user:
            user = User.objects.get(email=email)

            already_voted = Candidates.objects.filter(voted_by=user)
            print(already_voted)

            if already_voted and not user.is_superuser:
                print(already_voted)
                data = "you already voted for the candidates"
                return render(request, 'basic_auth/otp.html', {'already_voted': data})

            otp_creation(user, user.email)
            request.session['email'] = email
            return render(request, 'basic_auth/otp.html')
        else:
            error = "invalid login"
            return render(request, 'basic_auth/login.html', {'error': error})
    else:
        now = datetime.now()

        voting_time = VotingTime.objects.get(is_active=True)
        start_timing = voting_time.start_time + timedelta(hours=5, minutes=30)
        start_time_format = start_timing.strftime('%m/%d/%Y %H:%M:%S')
        end_timing = voting_time.end_time + timedelta(hours=5, minutes=30)
        end_time_format = end_timing.strftime('%m/%d/%Y %H:%M:%S')

        current_time = now.strftime('%m/%d/%Y %H:%M:%S')
        if start_time_format > current_time:
            vote = "Voting not yet started"
            return render(request, 'basic_auth/login.html', {'data': vote})
        elif current_time > end_time_format:
            vote = "Voting finished"
            return render(request, 'basic_auth/login.html', {'data': vote})
        return render(request, 'basic_auth/login.html')


@csrf_exempt
def verify_otp(request):
    if request.method == 'POST':
        email = request.session['email']
        # email = request.POST['email']
        otp = request.POST['otp']
        print(otp)

        try:
            user_obj = User.objects.get(email=email)
        except User.DoesNotExist:
            return HttpResponse("invalid user")

        if otp == '':
            data = "invalid otp"
            return render(request, 'basic_auth/otp.html', {'error': data})
        otp_obj = user_obj.otp_set.first()

        expiry = otp_obj.updated_at + timedelta(minutes=5) > datetime.now(pytz.utc)
        # print(otp_obj.updated_at + datetime.timedelta(minutes=5), datetime.datetime.now(pytz.utc))

        if not expiry:
            data = "OTP Expired / Invalid"
            return render(request, 'basic_auth/otp.html', {'error': data})

        validate_otp = OTP.objects.filter(otp_code=otp, used=True)
        print(validate_otp)
        if validate_otp:
            return render(request, 'basic_auth/otp.html')

        if otp_obj.otp_code == int(otp):
            otp_obj = user_obj.otp_set.values('otp_code').first()
            if otp_obj['otp_code'] == int(otp):
                user = authenticate(request, email=email, otp=otp)
                if user is not None:
                    login(request, user)

                    return redirect('/poll/voting')
                else:
                    return render(request, 'basic_auth/otp.html')
    error = "invalid otp"
    print("No")
    return render(request, 'basic_auth/otp.html', {'error': error})


@login_required
@csrf_exempt
def submitvote(request):
    if request.method == 'POST':
        form = NameList(request.POST)
        if form.is_valid():
            data = form.save(commit=False)
            data.voted_by = request.user
            data.save()
            return redirect('/poll/logout/')
        else:
            form = NameList()
    user = User.objects.get(email=request.user)
    already_voted = Candidates.objects.filter(voted_by=user)
    print(already_voted)

    if already_voted:
        print(already_voted)
        data = "you already voted for the candidates"
        return render(request, 'basic_auth/otp.html', {'already_voted': data})

    now = datetime.now()

    voting_time = VotingTime.objects.get(is_active=True)
    start_timing = voting_time.start_time + timedelta(hours=5, minutes=30)
    start_time_format = start_timing.strftime('%m/%d/%Y %H:%M:%S')
    end_timing = voting_time.end_time + timedelta(hours=5, minutes=30)
    end_time_format = end_timing.strftime('%m/%d/%Y %H:%M:%S')

    current_time = now.strftime('%m/%d/%Y %H:%M:%S')
    if start_time_format > current_time:
        vote = "Voting not yet started"
        return render(request, 'basic_auth/voting.html', {'data': vote})
    elif current_time > end_time_format:
        vote = "Voting finished"
        return render(request, 'basic_auth/voting.html', {'data': vote})

    form = NameList()
    return render(request, 'basic_auth/voting.html', {'form': form})


@login_required
def count(request):
    user = request.user
    if not user.is_superuser:
        return render(request, 'basic_auth/results.html', {'error': "you don't have permission"})
    var = Candidates.objects.values_list('candidates_name', flat=True)
    no_of_lists_per_name = Counter(chain.from_iterable(map(set, var)))

    results = {name: no_of_lists for name, no_of_lists in no_of_lists_per_name.most_common()}
    print(results)
    return render(request, 'basic_auth/results.html', {'data': results})


def userlogout(request):
    logout(request)
    return redirect('/poll/')



# def votetiming(request):
#     if request.method == 'POST':
#         form = VoteTimingForm(request.POST)
#         print(request.POST['start_time'])
#
#         start_timing = request.POST['start_time']
#         end_timing = request.POST['end_time']
#         schedule = VotingTime()
#         print(type(start_timing))
#         schedule.start_time = start_timing
#         schedule.end_time = end_timing
#         schedule.is_active = True
#         schedule.save()
#
#         return redirect('/poll/')
#     form = VoteTimingForm()
#     return render(request, 'basic_auth/votetime.html', {'form': form})
